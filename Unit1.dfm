object Form1: TForm1
  Left = 0
  Top = 0
  ActiveControl = EdPosicion
  BorderIcons = [biSystemMenu]
  Caption = 'Tratamiento de Cadenas con Delimitador'
  ClientHeight = 141
  ClientWidth = 634
  Color = clBtnFace
  Constraints.MaxHeight = 180
  Constraints.MaxWidth = 650
  Constraints.MinHeight = 180
  Constraints.MinWidth = 650
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BtParsearHasta: TButton
    Left = 16
    Top = 99
    Width = 78
    Height = 35
    Caption = 'Parsear hasta la Posicion'
    TabOrder = 0
    TabStop = False
    WordWrap = True
    OnClick = BtParsearHastaClick
  end
  object EdCadena: TLabeledEdit
    Left = 100
    Top = 16
    Width = 445
    Height = 21
    TabStop = False
    EditLabel.Width = 57
    EditLabel.Height = 16
    EditLabel.Caption = 'Cadena :'
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clBlack
    EditLabel.Font.Height = -13
    EditLabel.Font.Name = 'Tahoma'
    EditLabel.Font.Style = [fsBold]
    EditLabel.ParentFont = False
    LabelPosition = lpLeft
    TabOrder = 1
    Text = 
      'algun texto|otro texto diferente|mas texto|para variar mas texto' +
      '|por fin el ultimo texto|'
  end
  object EdResultado: TLabeledEdit
    Left = 100
    Top = 44
    Width = 445
    Height = 21
    TabStop = False
    Color = 8454143
    EditLabel.Width = 74
    EditLabel.Height = 16
    EditLabel.Caption = 'Resultado :'
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clBlack
    EditLabel.Font.Height = -13
    EditLabel.Font.Name = 'Tahoma'
    EditLabel.Font.Style = [fsBold]
    EditLabel.ParentFont = False
    LabelPosition = lpLeft
    ReadOnly = True
    TabOrder = 2
  end
  object EdPosicion: TLabeledEdit
    Left = 258
    Top = 72
    Width = 45
    Height = 21
    EditLabel.Width = 61
    EditLabel.Height = 16
    EditLabel.Caption = 'Posici'#243'n :'
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clBlack
    EditLabel.Font.Height = -13
    EditLabel.Font.Name = 'Tahoma'
    EditLabel.Font.Style = [fsBold]
    EditLabel.ParentFont = False
    LabelPosition = lpLeft
    TabOrder = 3
  end
  object BtEliminarToken: TButton
    Left = 439
    Top = 71
    Width = 106
    Height = 25
    Caption = 'Eliminar Token'
    TabOrder = 4
    OnClick = BtEliminarTokenClick
  end
  object BtObtenerToken: TButton
    Left = 309
    Top = 70
    Width = 106
    Height = 25
    Caption = 'Obtener Token'
    TabOrder = 5
    OnClick = BtObtenerTokenClick
  end
  object EdCantTokens: TLabeledEdit
    Left = 100
    Top = 72
    Width = 45
    Height = 21
    TabStop = False
    Color = 8454143
    EditLabel.Width = 92
    EditLabel.Height = 16
    EditLabel.Caption = 'Cant. Tokens :'
    EditLabel.Font.Charset = DEFAULT_CHARSET
    EditLabel.Font.Color = clBlack
    EditLabel.Font.Height = -13
    EditLabel.Font.Name = 'Tahoma'
    EditLabel.Font.Style = [fsBold]
    EditLabel.ParentFont = False
    LabelPosition = lpLeft
    ReadOnly = True
    TabOrder = 6
  end
  object BtParsear: TButton
    Left = 194
    Top = 106
    Width = 75
    Height = 25
    Caption = 'Parsear'
    TabOrder = 7
    OnClick = BtParsearClick
  end
  object BtObtenerCadaToken: TButton
    Left = 328
    Top = 99
    Width = 87
    Height = 35
    Caption = 'Obtener Cada Token'
    TabOrder = 8
    WordWrap = True
    OnClick = BtObtenerCadaTokenClick
  end
end
