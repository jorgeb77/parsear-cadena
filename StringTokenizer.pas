unit StringTokenizer;

interface

uses
  SysUtils, StrUtils;
type
  TStringTokens = array of string;

  function GetTokens(Cadena, Separador : string) : TStringTokens;
  function GetToken(Cadena, Separador : string; Token : Integer) : string;
  function GetTokenCount(Cadena, Separador : string) : Integer;
  function IsLastToken(Cadena, Separador : string; Token : Integer) : Boolean;

implementation

function GetTokens(Cadena,Separador : string) : TStringTokens;
var
  Tokens : TStringTokens;
  I, TotalTokens : Integer;
begin
  TotalTokens := GetTokenCount(Cadena, Separador);
  SetLength(Tokens, TotalTokens);
  for I:= 0 to TotalTokens - 1 do
     Tokens[I]:= GetToken(Cadena, Separador, I+1);
  Result := Tokens;
end;

function GetToken(Cadena, Separador : string; Token : Integer) : string;
var
  Posicion : Integer;
begin
  while Token > 1 do
    begin
      Delete(Cadena,1,Pos(Separador,Cadena));
      Dec(Token);
    end;

  Posicion:= Pos(Separador,Cadena);
  if Posicion = 0 then
    Result:= TrimLeft(TrimRight(Cadena))
  else
    Result:= TrimLeft(TrimRight(Copy(Cadena,1,Posicion-Length(Separador))));
end;

function GetTokenCount(Cadena, Separador : string) : Integer;
var
  Posicion : Integer;
begin
  Posicion := Pos(Separador,Cadena);
  Result   := 1;

  if Cadena <> '' then
    begin
      if Posicion <> 0 then
        while Posicion <> 0 do
          begin
            Delete(Cadena,1,Posicion);
            Posicion := Pos(Separador,Cadena);
            Inc(Result);
          end;
    end
  else
    Result:= 0;
end;

//Para saber si el numero de token es el ultimo en la cadena delimitada...
function IsLastToken(Cadena, Separador : string; Token : Integer) : Boolean;
var
  P : Integer;
begin
  if Token = GetTokenCount(Cadena, Separador) then
    Result := True
  else
    Result := False;
end;

end.
