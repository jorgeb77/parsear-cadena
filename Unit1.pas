unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TForm1 = class(TForm)
    BtParsearHasta: TButton;
    EdCadena: TLabeledEdit;
    EdResultado: TLabeledEdit;
    EdPosicion: TLabeledEdit;
    BtEliminarToken: TButton;
    BtObtenerToken: TButton;
    EdCantTokens: TLabeledEdit;
    BtParsear: TButton;
    BtObtenerCadaToken: TButton;
    procedure BtParsearHastaClick(Sender: TObject);
    procedure BtEliminarTokenClick(Sender: TObject);
    procedure BtObtenerTokenClick(Sender: TObject);
    procedure BtParsearClick(Sender: TObject);
    procedure BtObtenerCadaTokenClick(Sender: TObject);
  private
    function Borrar(C : string; P,N : Integer) : string;
    function NPos(Cadena : string; SubCadena : string; Posicion : Integer) : Integer;
    function GetToken(Cadena, Separador : String; Token : Integer) : string;
    function DeleteToken(Cadena, Separador : string; Token : Integer) : string;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses untString, StringTokenizer;

{$R *.dfm}

function TForm1.NPos(Cadena : string; SubCadena : string; Posicion : Integer) : Integer;
var
  Temp : string;
  N, I : Integer;
begin
  N:= Posicion;
  Temp:= '';
  I:= 1;
  while I <= N do
    begin
      Posicion:= Pos(SubCadena, Cadena);
      if Posicion <> 0 then
        begin
          Temp:= Temp + Copy(Cadena, 1, Posicion);
          Delete(Cadena, 1, Posicion);
        end
      else
        Temp:= Cadena;
        Inc(I);
    end;
  Result:= Length(Temp);
end;

//Eliminar de una cadena, los n caracteres que aparecen a partir de la posicion p
function TForm1.Borrar(C : string; P,N : Integer) : string;
begin
  if (P <= 0) or (N <= 0) then
    Result := C
  else
    Result := Copy(C,1,P-1) + Copy(C,P+N,Length(C));

  ShowMessage( Copy(C,1,P-1) +#13+
               Copy(C,P+N,Length(C)) );
end;

procedure TForm1.BtParsearHastaClick(Sender: TObject);
var
  Cadena : string;
  Posicion : Integer;
begin
  Cadena           := EdCadena.Text;
  Posicion         := StrToIntDef(EdPosicion.Text,0);
  EdResultado.Text := Borrar(Cadena, NPos(Cadena,'|',Posicion), Length(Cadena));

//  ShowMessage(Copy(Texto, Pos('|',Texto), Length(Texto)));

//  ShowMessage(Copy(Texto, NPos(Texto,'|',2)+1, Length(Texto)));

end;

procedure TForm1.BtParsearClick(Sender: TObject);
var
  Tokens : TStringTokens;
  Cadena : string;
  I, TotalTokens : Integer;
begin
  Cadena      := Trim(EdCadena.Text);
  Tokens      := GetTokens(Cadena, '|');
  TotalTokens := GetTokenCount(Cadena, '|');

  for I:= 1 to TotalTokens do
    begin
      Tokens[I]:= GetToken(Cadena, '|', I);
      Showmessage(Tokens[I]);
    end;

//  // para la inserción
//  with QryInsercion do
//    begin
//      Close;
//      SQL.Clear;
//      SQL.Add('INSERT INTO LATABLA (clave,nombre,folio,campoN)');
//      SQL.Add('VALUES(:clave,:nombre,:folio,:campoN)');
//      ParamByName('clave').AsString   := Tokens[0];
//      ParamByName('nombre').AsString  := Tokens[1];
//      ParamByName('folio,:').AsString := Tokens[2];
//      ParamByName('campoN').AsString  := Tokens[3];
//      ExecSql;
//    end;


end;

procedure TForm1.BtEliminarTokenClick(Sender: TObject);
var
  Cadena, Delimitador : string;
begin
  Cadena      := EdCadena.Text;
  Delimitador := '|';
  EdResultado.Text := DeleteToken(Cadena, Delimitador,  StrToInt(EdPosicion.Text));
end;

procedure TForm1.BtObtenerCadaTokenClick(Sender: TObject);
var
 Cadena, Caracter, Texto : string;
 Cant, I : Integer;
begin
  Cadena := EdCadena.Text;
  Cant   := Length(Cadena);
  for I:= 1 to Cant do
    begin
      Caracter := Cadena[I];  //igual a Caracter := Copy(Cadena,I,1);

      // Comparo si el caracter es igual a | si es asi, que me lo muestre.
      if Caracter = '|' then
        begin
          ShowMessage(Texto);
          Texto:= '';
        end
      else  // si no voy armando mi texto que desarme ya separado por |
        begin
          Texto:= Texto + Caracter;
        end;
    end;

end;

procedure TForm1.BtObtenerTokenClick(Sender: TObject);
var
  Pos : Integer;
begin
  Pos               := StrToIntDef(EdPosicion.Text,0);
  EdResultado.Text  := ObtenerToken(EdCadena.Text,'|',Pos);
  EdCantTokens.Text := IntToStr(CantidadToken(EdCadena.Text,'|'));

//  //usando la unit StringTokenizer
//  EdResultado.Text  := GetToken(Trim(EdCadena.Text), '|', StrToInt(EdPosicion.Text));
//  EdCantTokens.Text := IntToStr(GetTokenCount(EdCadena.Text,'|'));

end;

function TForm1.GetToken(Cadena, Separador : String; Token : Integer) : string;
var
 Posicion : Integer;
begin
  while Token > 1 do
    begin
      Delete(Cadena, 1, Pos(Separador,Cadena)+Length(Separador)-1);
      Dec(Token);
    end;

  Posicion:= Pos(Separador, Cadena);

  if Posicion = 0 then
    Result := Cadena
  else
    Result:= Copy(Cadena, 1, Posicion-1);
end;

//Para eliminar el numero de token dentro de la cadena delimitada...
function TForm1.DeleteToken(Cadena, Separador : string; Token : Integer) : string;
var
  Temp : string;
begin
  Temp := Cadena;
  Delete(Temp, Pos(GetToken(Temp, Separador, Token), Temp), Length(GetToken(Temp, Separador, Token))+1);
  Result := Temp;
end;

end.
