unit untString;

interface

uses SysUtils;

{ Retorna la posici�n de la primer ocurrencia del car�cter c dentro del string s
  contando a partir del car�cter offset.
  Si s no contiene ning�n car�cter c entonces retorna un valor negativo. }
function IndexOf(s : string; c : Char; Offset : Integer) : Integer;

{ Retorna la subcadena de s comprendida entre los caracteres cuyas posiciones son
 desde (inclusive) y hasta (no inclusive). }
function Copiar(Cadena : string; Desde, Hasta : Integer) : string;

// Retorna true si s es vac�o (''), o false si tiene al menos alg�n car�cter
function IsEmpty(s : string) : Boolean;

//Retorna el token que se encuentra en la posici�n i
function ObtenerToken(s : string; sep : Char; I : Integer) : string;

//Cuenta la cantidad de tokens contenidos en s
function CantidadToken(Cadena : string; Sep : Char) : Integer;


implementation

function IsEmpty(s : string) : Boolean;
begin
  Result:= Length(s) = 0;
end;

 { EJEMPLO DE TOKEN :  ESTO|ES|UNA|PRUEBA  }
function ObtenerToken(s : string; sep : Char; I : Integer) : string;

{ Considera al string s como varios strings separados por el separador sep
  permitiendo iterarlo y obtener cada token (substring entre sep y sep)
  contenido en s. }
  function Tokenizer(s : string; sep : Char; var buff : string; var offset : Integer) : Boolean;
  var
    Desde, Hasta : Integer;
  begin
    if (offset > Length(s)) then
      begin
        Result:= False;
        Exit;
      end;

    Desde := offset;
    Hasta := IndexOf(s,sep,Desde);

    if (Hasta < 0) then
     begin
       Hasta:= Length(s) + 1;
     end;

    offset := Hasta + 1;
    buff   := Copiar(s,Desde,Hasta);
    Result := True;
  end;

var
  x, offset : Integer;
  buff : string;
begin
  offset := 1;
  for x:= 1 to i do
    begin
      Tokenizer(s,sep,buff,offset);
    end;
  Result:= buff;
end;

function CantidadToken(Cadena : string; Sep : Char) : Integer;
var
  I, N, Cont : Integer;
begin
  N    := Length(Cadena);
  I    := 0;
  Cont := 1;
  while (I <= N) do
    begin
      if (Cadena[I] = Sep) then
        begin
          Cont:= Cont + 1;
        end;
      I:= I + 1;
    end;
  Result:= Cont;
end;

function IndexOf(s : string; c : Char; offset : Integer) : Integer;
var
  I : Integer;
begin
  I:= offset;
  while ((I <= Length(s)) and (s[I] <> c)) do
    begin
      I:= I + 1;
    end;

  if (I <= Length(s)) then
    begin
      Result := I;
    end
  else
    begin
      Result := -1;
    end;
end;

function Copiar(Cadena : string; Desde, Hasta : Integer) : string;
begin
  Result:= Copy(Cadena,Desde, Hasta-Desde);
end;


end.
